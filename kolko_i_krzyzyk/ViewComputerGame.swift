//
//  ViewComputerGame.swift
//  kolko_i_krzyzyk
//
//  Created by Karolina on 07.02.2018.
//  Copyright © 2018 Hennis. All rights reserved.
//

import Foundation
import UIKit

class ViewComputerGame: UIViewController {
    var pola = [0,0,0,0,0,0,0,0,0]
    var gracz=1
    var koniec_gry = false
    

   var wygrane_gracz_1=0
   var wygrane_gracz_2=0
    
    var g1 = false
    var g2 = false
    
    @IBOutlet var g1Point: UILabel!
    @IBOutlet var g2Point: UILabel!
    
    
    @IBOutlet var nazwa_wygranego: UILabel!
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    @IBOutlet var button4: UIButton!
    @IBOutlet var button5: UIButton!
    @IBOutlet var button6: UIButton!
    @IBOutlet var button7: UIButton!
    @IBOutlet var button8: UIButton!
    @IBOutlet var button9: UIButton!
    var koniecRuchu = false
    

    @IBAction func ZaznaczPole(_ sender: AnyObject) {
        if(gracz==1 && pola[sender.tag-1]==0 && koniec_gry==false )
        {
            sender.setImage(UIImage(named: "krzyzyk.png"), for: UIControl.State())
            pola[sender.tag-1]=1
            gracz=2
            if (pola[0]==1 && pola[1]==1 && pola[2]==1) || (pola[3]==1 && pola[4]==1 && pola[5]==1) || (pola[6]==1 && pola[7]==1 && pola[8]==1) || (pola[0]==1 && pola[3]==1 && pola[6]==1) || (pola[1]==1 && pola[4]==1 && pola[7]==1) || (pola[2]==1 && pola[5]==1 && pola[8]==1) || (pola[2]==1 && pola[4]==1 && pola[6]==1) || (pola[0]==1 && pola[4]==1 && pola[8]==1)
            { koniec_gry=true
                g1=true
                nazwa_wygranego.text="WYGRAŁ GRACZ"
                nazwa_wygranego.isHidden=false
                nastepnaGra.isHidden=false
            }
            
           
            if pola[0] != 0 && pola[1] != 0 && pola[2] != 0 && pola[3] != 0 && pola[4] != 0 && pola[5] != 0 && pola[6] != 0 && pola[7] != 0 && pola[8] != 0
            {
                nazwa_wygranego.text="REMIS"
                nazwa_wygranego.isHidden=false
                nastepnaGra.isHidden=false
            }
            koniecRuchu = true
        }
        
        if(gracz==2 && koniec_gry==false && koniec_gry==false && koniecRuchu==true && (pola[0]==0 || pola[1]==0 || pola[2]==0 || pola[3]==0 || pola[4]==0 || pola[5]==0 || pola[6]==0 || pola[7]==0 || pola[8]==0))
        {
            
            //WYGRANA
            // 1 i 2
            if(pola[0] == 2 && pola[2]==1 && pola[2]==0)
            {button3.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[2]=2
                gracz=1
            }
                //1 i 3
            else if(pola[0] == 2 && pola[2]==2 && pola[1]==0)
            {
                button2.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[1]=2
                gracz=1
            }
                //2 i 3
            else if(pola[1] == 2 && pola[2]==2 && pola[0]==0)
            {
                button1.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[0]=2
                gracz=1
            }
                //4 i 5
            else if(pola[3] == 2 && pola[4] == 2 && pola[5] == 0)
            {
                button6.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[5]=2
                gracz=1
            }
                //4 i 6
            else if(pola[3] == 2 && pola[5] == 2 && pola[4] == 0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
            }
                //5 i 6
            else if(pola[4] == 2 && pola[5]==2 && pola[3]==0)
            {
                button4.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[3]=2
                gracz=1
            }
                //7 i 8
            else if(pola[6] == 2 && pola[7] == 2 && pola[8] == 0)
            {
                button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[8]=2
                gracz=1
            }
                //7 i 9
            else if(pola[6] == 2 && pola[8]==2 && pola[7]==0)
            {
                button8.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[7]=2
                gracz=1
            }
                //8 i 9
            else if(pola[7] == 2 && pola[8]==2 && pola[6]==0)
            {
                button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[6]=2
                gracz=1
            }
                //1 i 4
            else if(pola[0] == 2 && pola[3]==2 && pola[6]==0)
            {
                button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[6]=2
                gracz=1
            }
                //2 i 5
            else if(pola[1] == 2 && pola[4]==2 && pola[7]==0)
            {
                button8.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[7]=2
                gracz=1
            }
                //3 i 6
            else if(pola[2] == 2 && pola[5]==2 && pola[8]==0)
            {
                button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[8]=2
                gracz=1
            }
                //1 i 7
            else if(pola[0] == 2 && pola[6]==2 && pola[3]==0)
            {
                button4.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[3]=2
                gracz=1
            }
                //2 i 8
            else if(pola[1] == 2 && pola[7]==2 && pola[4]==0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
            }
                //3 i 9
            else if(pola[2] == 2 && pola[8]==2 && pola[5]==0)
            {
                button6.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[5]=2
                gracz=1
            }
                //1 i 9
            else if(pola[0]==2 && pola[8]==2 && pola[4]==0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
            }
                //3 i 7
            else if(pola[2]==2 && pola[6]==2 && pola[4]==0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
                
            }
                //5 i 9
            else if(pola[4]==2 && pola[8]==2 && pola[0]==0)
            {
                button1.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[0]=2
                gracz=1
            }
                //5 i 7
            else if(pola[4]==2 && pola[6]==2 && pola[2]==0)
            {
                button3.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[2]=2
                gracz=1
            }
                //5 i 1
            else if(pola[4]==2 && pola[0]==2 && pola[8]==0)
            {
                button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[8]=2
                gracz=1
            }
                //5 i 3
            else if(pola[4]==2 && pola[2]==2 && pola[6]==0)
            {
                button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[6]=2
                gracz=1
            }
                //5 i 8
            else if(pola[4]==2 && pola[7]==2 && pola[1]==0)
            {
                button2.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[1]=2
                gracz=1
            }
            
            //BLOKADA
            //1 i 2
            else if(pola[0] == 1 && pola[1]==1 && pola[2]==0)
            {button3.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[2]=2
                gracz=1
            }
            //1 i 3
            else if(pola[0] == 1 && pola[2]==1 && pola[1]==0)
            {
                button2.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[1]=2
                gracz=1
            }
            //2 i 3
            else if(pola[1] == 1 && pola[2]==1 && pola[0]==0)
            {
                button1.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[0]=2
                gracz=1
            }
            //4 i 5
            else if(pola[3] == 1 && pola[4] == 1 && pola[5] == 0)
            {
                button6.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[5]=2
                gracz=1
            }
            //4 i 6
            else if(pola[3] == 1 && pola[5] == 1 && pola[4] == 0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
            }
            //5 i 6
            else if(pola[4] == 1 && pola[5]==1 && pola[3]==0)
            {
                button4.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[3]=2
                gracz=1
            }
            //7 i 8
            else if(pola[6] == 1 && pola[7] == 1 && pola[8] == 0)
            {
                button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[8]=2
                gracz=1
            }
            //7 i 9
            else if(pola[6] == 1 && pola[8]==1 && pola[7]==0)
            {
                button8.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[7]=2
                gracz=1
            }
            //8 i 9
            else if(pola[7] == 1 && pola[8]==1 && pola[6]==0)
            {
                button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[6]=2
                gracz=1
            }
            //1 i 4
            else if(pola[0] == 1 && pola[3]==1 && pola[6]==0)
            {
                button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[6]=2
                gracz=1
            }
            //2 i 5
            else if(pola[1] == 1 && pola[4]==1 && pola[7]==0)
            {
                button8.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[7]=2
                gracz=1
            }
            //3 i 6
            else if(pola[2] == 1 && pola[5]==1 && pola[8]==0)
            {
                button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[8]=2
                gracz=1
            }
            //1 i 7
            else if(pola[0] == 1 && pola[6]==1 && pola[3]==0)
            {
                button4.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[3]=2
                gracz=1
            }
            //2 i 8
            else if(pola[1] == 1 && pola[7]==1 && pola[4]==0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
            }
            //3 i 9
            else if(pola[2] == 1 && pola[8]==1 && pola[5]==0)
            {
                button6.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[5]=2
                gracz=1
            }
            //1 i 9
            else if(pola[0]==1 && pola[8]==1 && pola[4]==0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
            }
            //3 i 7
            else if(pola[2]==1 && pola[6]==1 && pola[4]==0)
            {
                button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[4]=2
                gracz=1
                
            }
            //5 i 9
            else if(pola[4]==1 && pola[8]==1 && pola[0]==0)
            {
                button1.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[0]=2
                gracz=1
            }
            //5 i 7
            else if(pola[4]==1 && pola[6]==1 && pola[2]==0)
            {
                button3.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[2]=2
                gracz=1
            }
            //5 i 1
            else if(pola[4]==1 && pola[0]==1 && pola[8]==0)
            {
                button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[8]=2
                gracz=1
            }
                //5 i 3
            else if(pola[4]==1 && pola[2]==1 && pola[6]==0)
            {
                button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[6]=2
                gracz=1
            }
                //5 i 8
            else if(pola[4]==1 && pola[7]==1 && pola[1]==0)
            {
                button2.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                pola[1]=2
                gracz=1
            }
            else
            {
               var znalezione = false
                var rand: Int
                while(znalezione==false)
                {
                    rand = Int(arc4random_uniform(9))
                    if(pola[rand]==0)
                    {
                      if(rand==0)
                      {button1.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                        pola[0]=2
                        gracz=1}
                        if(rand==1)
                        {
                            button2.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[1]=2
                            gracz=1
                        }
                        if(rand==2)
                        {
                            button3.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[2]=2
                            gracz=1
                        }
                        if(rand==3)
                        {button4.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[3]=2
                            gracz=1}
                        if(rand==4)
                        {
                            button5.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[4]=2
                            gracz=1
                        }
                        if(rand==5)
                        {button6.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[5]=2
                            gracz=1}
                        if(rand==6)
                        {button7.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[6]=2
                            gracz=1}
                        if(rand==7)
                        {button8.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[7]=2
                            gracz=1}
                        if(rand==8)
                        {button9.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
                            pola[8]=2
                            gracz=1}
                        znalezione=true
                    }
                }
                if pola[0]==2 && pola[1]==2 && pola[2]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[3]==2 && pola[4]==2 && pola[5]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                
                if pola[6]==2 && pola[7]==2 && pola[8]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[0]==2 && pola[3]==2 && pola[6]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[1]==2 && pola[4]==2 && pola[7]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[2]==2 && pola[5]==2 && pola[8]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[0]==2 && pola[4]==2 && pola[8]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[2]==2 && pola[4]==2 && pola[6]==2
                { koniec_gry=true
                    g2=true
                    nazwa_wygranego.text="WYGRAŁ KOMPUTER"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                if pola[0] != 0 && pola[1] != 0 && pola[2] != 0 && pola[3] != 0 && pola[4] != 0 && pola[5] != 0 && pola[6] != 0 && pola[7] != 0 && pola[8] != 0
                {
                    nazwa_wygranego.text="REMIS"
                    nazwa_wygranego.isHidden=false
                    nastepnaGra.isHidden=false
                }
                 znalezione=false
            }
            
            koniecRuchu=false
            if pola[0] != 0 && pola[1] != 0 && pola[2] != 0 && pola[3] != 0 && pola[4] != 0 && pola[5] != 0 && pola[6] != 0 && pola[7] != 0 && pola[8] != 0
            {
                nazwa_wygranego.text="REMIS"
                nazwa_wygranego.isHidden=false
                nastepnaGra.isHidden=false
            }
        }
        if pola[0]==1 && pola[1]==1 && pola[2]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        if pola[3]==1 && pola[4]==1 && pola[5]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        
        if pola[6]==1 && pola[7]==1 && pola[8]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        if pola[0]==1 && pola[3]==1 && pola[6]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        if pola[1]==1 && pola[4]==1 && pola[7]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        if pola[2]==1 && pola[5]==1 && pola[8]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        if pola[0]==1 && pola[4]==1 && pola[8]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            
        }
        if pola[2]==1 && pola[4]==1 && pola[6]==1
        { koniec_gry=true
            g1=true
            nazwa_wygranego.text="WYGRAŁ GRACZ"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        if pola[0] != 0 && pola[1] != 0 && pola[2] != 0 && pola[3] != 0 && pola[4] != 0 && pola[5] != 0 && pola[6] != 0 && pola[7] != 0 && pola[8] != 0
        {
            nazwa_wygranego.text="REMIS"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        //gracz2
        if (pola[0]==2 && pola[1]==2 && pola[2]==2) || (pola[3]==2 && pola[4]==2 && pola[5]==2) || (pola[6]==2 && pola[7]==2 && pola[8]==2) || (pola[0]==2 && pola[3]==2 && pola[6]==2) || (pola[1]==2 && pola[4]==2 && pola[7]==2) || (pola[2]==2 && pola[5]==2 && pola[8]==2) || (pola[2]==2 && pola[4]==2 && pola[6]==2) || (pola[0]==2 && pola[4]==2 && pola[8]==2)
        { koniec_gry=true
            g2=true
            nazwa_wygranego.text="WYGRAŁ KOMPUTER"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        }
    
  

    @IBOutlet var nastepnaGra: UIButton!
    @IBAction func START(_ sender: AnyObject)
    {
        nastepnaGra.isHidden=true
        nazwa_wygranego.isHidden=true
        pola = [0,0,0,0,0,0,0,0,0]
        koniec_gry = false
        if(g1==true)
        {
            wygrane_gracz_1=wygrane_gracz_1+1
        }
        if(g2==true)
        {
            wygrane_gracz_2=wygrane_gracz_2+1
        }
        for i in 1...9
        {
            let button = view.viewWithTag(i) as! UIButton
            button.setImage(nil, for: UIControl.State())
        }
        g1Point.text="\(wygrane_gracz_1)"
        g2Point.text="\(wygrane_gracz_2)"
        g1=false
        g2=false
        
    }
    
    @IBAction func Reset(_ sender: Any) {
    for i in 1...9
    {
    let button = view.viewWithTag(i) as! UIButton
    button.setImage(nil, for: UIControl.State())
        }}
    
    @IBAction func ResetStatystyk(_ sender: Any) {
        wygrane_gracz_1=0
        wygrane_gracz_2=0
        g1Point.text="\(wygrane_gracz_1)"
        g2Point.text="\(wygrane_gracz_2)"
    }
    
    
    }

