//
//  ViewController.swift
//  kolko_i_krzyzyk
//
//  Created by Karolina on 03.02.2018.
//  Copyright © 2018 Hennis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var gracz=1 //krzyzyk
    var pola = [0,0,0,0,0,0,0,0,0]
    let wygrane = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]]
    var koniec_gry=true
    var wygrane_gracz_1 = 0
    var wygrane_gracz_2 = 0

    @IBOutlet var wpisanaNazwaG1: UITextField!
    @IBOutlet var wpisanaNazwaG2: UITextField!
    
    @IBOutlet var RuchGracza: UILabel!
    @IBOutlet var nazwa_wygranego: UILabel!
    @IBOutlet var nastepnaGra: UIButton!
    @IBOutlet var g1: UILabel!
    @IBOutlet var g2: UILabel!
    @IBOutlet var label_G1: UILabel!
    @IBOutlet var label_G2: UILabel!
    static var temp = "a"
    static var temp2 = "b"
    
    @IBAction func zatwierdz(_ sender: Any) {
        ViewController.temp = wpisanaNazwaG1.text!
        ViewController.temp2 = wpisanaNazwaG2.text!
    }
    
    
    @IBAction func startGRY(_ sender: AnyObject) {
        label_G1.text = ViewController.temp
        label_G2.text = ViewController.temp2
        nastepnaGra.isHidden=true
         koniec_gry=false
         nazwa_wygranego.isHidden=true
         RuchGracza.text="Teraz ruch \(ViewController.temp)"
    }
    
    @IBAction func action(_ sender: AnyObject) {
      
        if(pola[sender.tag-1]==0 && koniec_gry==false){
           
            pola[sender.tag-1]=gracz
        if(gracz==1)
        {sender.setImage(UIImage(named: "krzyzyk.png"), for: UIControl.State())
            RuchGracza.text="Teraz ruch \(ViewController.temp2)"
            gracz=2

        }
        else
        {
            RuchGracza.text="Teraz ruch \(ViewController.temp)"
            sender.setImage(UIImage(named: "kolko.png"), for: UIControl.State())
            gracz=1
            }
            
        }
        
        
        if (pola[0]==1 && pola[1]==1 && pola[2]==1) || (pola[3]==1 && pola[4]==1 && pola[5]==1) || (pola[6]==1 && pola[7]==1 && pola[8]==1) || (pola[0]==1 && pola[3]==1 && pola[6]==1) || (pola[1]==1 && pola[4]==1 && pola[7]==1) || (pola[2]==1 && pola[5]==1 && pola[8]==1) || (pola[2]==1 && pola[4]==1 && pola[6]==1) || (pola[0]==1 && pola[4]==1 && pola[8]==1)
        { koniec_gry=true
            wygrane_gracz_1=wygrane_gracz_1+1
            nazwa_wygranego.text="WYGRAŁ GRACZ \(ViewController.temp)"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
            
        }
        
        //gracz 2
        if (pola[0]==2 && pola[1]==2 && pola[2]==2) || (pola[3]==2 && pola[4]==2 && pola[5]==2) || (pola[6]==2 && pola[7]==2 && pola[8]==2) || (pola[0]==2 && pola[3]==2 && pola[6]==2) || (pola[1]==2 && pola[4]==2 && pola[7]==2) || (pola[2]==2 && pola[5]==2 && pola[8]==2) || (pola[2]==2 && pola[4]==2 && pola[6]==2) || (pola[0]==2 && pola[4]==2 && pola[8]==2)
        { koniec_gry=true
            wygrane_gracz_2=wygrane_gracz_2+1
            nazwa_wygranego.text="WYGRAŁ GRACZ \(ViewController.temp2)"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        
        if pola[0] != 0 && pola[1] != 0 && pola[2] != 0 && pola[3] != 0 && pola[4] != 0 && pola[5] != 0 && pola[6] != 0 && pola[7] != 0 && pola[8] != 0
        {
            nazwa_wygranego.text="REMIS"
            nazwa_wygranego.isHidden=false
            nastepnaGra.isHidden=false
        }
        
    }
    

    
    @IBAction func restart(_ sender: AnyObject) {
        pola = [0,0,0,0,0,0,0,0,0]
        koniec_gry = false
        for i in 1...9
        {
            let button = view.viewWithTag(i) as! UIButton
            button.setImage(nil, for: UIControl.State())
        }
    }
    
    @IBAction func nastepnaGra(_ sender: AnyObject) {
        pola = [0,0,0,0,0,0,0,0,0]
        koniec_gry = false
        for i in 1...9
        {
            let button = view.viewWithTag(i) as! UIButton
            button.setImage(nil, for: UIControl.State())
        }
        g1.text="\(wygrane_gracz_1)"
        g2.text="\(wygrane_gracz_2)"
    }
    
    @IBAction func resetStatystyk(_ sender: AnyObject) {
        wygrane_gracz_1=0
        wygrane_gracz_2=0
        g1.text="\(wygrane_gracz_1)"
        g2.text="\(wygrane_gracz_2)"
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

